#!/usr/bin/env python3
# coding=UTF-8

# =============================================================================
# titre           :tests.py
# description     :Solutionnaire des exercices sur les tests de routines
# author          :Louis Marchand
# date            :20240213
# version         :1.0
# usage           :import routines
# notes           :
# python_version  :3.4.0
# =============================================================================

import routines


def tests_aire_carre():
    """ Teste la fonction `aire_carre' """
    is_passed = True
    resultat = routines.aire_carre(132590)
    if resultat != 17580108100:
        is_passed = False
        print("Échec du cas normal de la routine aire_carree.")
    resultat = routines.aire_carre(0)
    if resultat != 0:
        is_passed = False
        print("Échec du cas limite de la routine aire_carree.")
    try:
        routines.aire_carre(-1)
        is_passed = False
        print("Échec du cas limite erronné de la routine aire_carree.")
    except:
        pass
    try:
        routines.aire_carre(-20532869)
        is_passed = False
        print("Échec du cas erronné (négatif) de la routine aire_carree.")
    except:
        pass
    try:
        routines.aire_carre("Bonjour")
        is_passed = False
        print("Échec du cas erronné (chaine) de la routine aire_carree.")
    except:
        pass
    try:
        routines.aire_carre([1, 2, 3])
        is_passed = False
        print("Échec du cas erronné (tableau) de la routine aire_carree.")
    except:
        pass
    if is_passed:
        print("Les tests de aire_carree ce sont terminés sans erreurs.")


def tests_minimum():
    """ Teste la fonction `minimum' """
    is_passed = True
    resultat = routines.minimum([79672572, 12377476, 58828207, 56714560,
                                 10676573, 325031, 59595098, 642727,
                                 43372534, 57457248])
    if resultat != 325031:
        is_passed = False
        print("Échec du cas normal de la routine minimum.")
    resultat = routines.minimum([78120171])
    if resultat != 78120171:
        is_passed = False
        print("Échec du cas limite de la routine minimum.")
    try:
        routines.minimum([])
        is_passed = False
        print("Échec du cas erronné limite de la routine minimum.")
    except:
        pass
    try:
        routines.minimum([1, 2, "3", 4])
        is_passed = False
        print("Échec du cas erronné (liste chaine) de la routine minimum.")
    except:
        pass
    try:
        routines.minimum("Bonjour")
        is_passed = False
        print("Échec du cas erronné (chaine) de la routine minimum.")
    except:
        pass
    try:
        routines.minimum(1)
        is_passed = False
        print("Échec du cas erronné (entier) de la routine minimum.")
    except:
        pass
    if is_passed:
        print("Les tests de minimum ce sont terminés sans erreurs.")


def tests_fibonacci():
    """ Teste la fonction `fibonacci' """
    is_passed = True
    resultat = routines.fibonacci(9)
    if resultat != 34:
        is_passed = False
        print("Échec du cas normal de la routine fibonacci.")
    resultat = routines.fibonacci(0)
    if resultat != 0:
        is_passed = False
        print("Échec du cas limite de la routine fibonacci.")
    try:
        routines.fibonacci(-1)
        is_passed = False
        print("Échec du cas limite erronné de la routine fibonacci.")
    except:
        pass
    try:
        routines.fibonacci(-23)
        is_passed = False
        print("Échec du cas erronné (négatif) de la routine fibonacci.")
    except:
        pass
    try:
        routines.fibonacci("Bonjour")
        is_passed = False
        print("Échec du cas erronné (chaine) de la routine fibonacci.")
    except:
        pass
    try:
        routines.fibonacci((1, 2, 3,))
        is_passed = False
        print("Échec du cas erronné (tuple) de la routine fibonacci.")
    except:
        pass
    if is_passed:
        print("Les tests de fibonacci ce sont terminés sans erreurs.")

if __name__ == "__main__":
    tests_aire_carre()
    tests_minimum()
    tests_fibonacci()
