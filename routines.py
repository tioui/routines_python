#!/usr/bin/env python3
# coding=UTF-8

# =============================================================================
# titre           :routines.py
# description     :Solutionnaire des exercices sur les routines
# author          :Louis Marchand
# date            :20160315
# version         :1.0
# usage           :import routines
# notes           :
# python_version  :3.4.0
# =============================================================================


def aire_carre(a_aire_cote):
    """
        Retourne l'aire d'un carré ayant `a_aire_cote' comme aire d'un côté.
        L'argument `a_aire_cote' doit être un entier positif.
    """
    l_result = 0
    if isinstance(a_aire_cote, int):
        if a_aire_cote >= 0:
            l_result = a_aire_cote * a_aire_cote
        else:
            raise ValueError("L'argument `a_aire_cote' doit être positif.")
    else:
        raise ValueError("L'argument `a_aire_cote' doit être un entier.")
    return l_result


def tout_element_est_entier(a_liste):
    """ Retourne vrai si tous les élément de `a_liste' est de type entier."""
    l_result = True
    for element in a_liste:
        if not isinstance(element, int):
            l_result = False
    return l_result


def minimum(a_liste):
    """ Retourne le minimum de la liste d'entier `a_liste'."""
    if tout_element_est_entier(a_liste):
        if len(a_liste) > 0:
            l_result = a_liste[0]
            for element in a_liste:
                if l_result > element:
                    l_result = element
        else:
            raise ValueError("L'argument `a_liste' ne doit pas être vide.")

    else:
        raise ValueError("L'argument `a_liste' doit être une liste d'entier.")
    return l_result


def fibonacci(a_nombre):
    """
        Calcul le `a_nombre' ième nombre de Fibonacci.
        `a_nombre' doit-être de type entier.
    """
    l_result = 0
    if isinstance(a_nombre, int):
        if a_nombre < 0:
            raise ValueError("L'argument `a_nombre' doit être positif.")
        else:
            if a_nombre < 2:
                l_result = a_nombre
            else:
                l_result = fibonacci(a_nombre - 1) + fibonacci(a_nombre - 2)
    else:
        raise ValueError("L'argument `a_nombre' doit être un entier.")
    return l_result
